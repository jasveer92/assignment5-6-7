import java.util.Date;

public class Hydro extends Bill {
	
	String agencyname;
	double unitconsumed;
	//constructor
	
	public  Hydro (int billid,String billdate,String billtype,double totalbillamount,String agencyname, double unitconsumed) {
		super (billid,billdate,billtype,totalbillamount);
		this.agencyname =agencyname;
		this.unitconsumed=unitconsumed;
	}
	//getter setter 
	public String getAgencyname() {
		return agencyname;
	}
	public void setAgencyname(String agencyname) {
		this.agencyname = agencyname;
	}
	public double getUnitconsumed() {
		return unitconsumed;
	}
	public void setUnitconsumed(double unitconsumed) {
		this.unitconsumed = unitconsumed;
	}
	
}

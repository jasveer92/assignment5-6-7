import java.util.ArrayList;
import java.util.Date;

public class Customer {
	

		
		int Customerid;
		String firstname;
		String lastname;
		String fullname; 
		String email;
		double totalamounttopay;
		ArrayList <Bill> billlist = new ArrayList<Bill>();
		//constructor
		public  Customer (int Customerid,String firstname,String lastname,String fullname,
		String email,double totalamounttopay)
		{
			this.Customerid=Customerid;
			this.firstname=firstname;
			this.lastname=lastname;
			this.fullname=fullname;
			this.email=email;
			this.totalamounttopay=totalamounttopay;
		}
		//getter setter 
		public int getCustomerid() {
			return Customerid;
		};
		public void setCustomerid(int customerid) {
			Customerid = customerid;
		}
		public String getFirstname() {
			return firstname;
		}
		public void setFirstname(String firstname) {
			this.firstname = firstname;
		}
		public String getLastname() {
			return lastname;
		}
		public void setLastname(String lastname) {
			this.lastname = lastname;
		}
		public String getFullname() {
			return fullname;
		}
		public void setFullname(String fullname) {
			this.fullname = fullname;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		
		public double getTotalamounttopay() {
			return totalamounttopay;
		}
		public void setTotalamounttopay(double totalamounttopay) {
			this.totalamounttopay = totalamounttopay;
		}
	// methods
		public void billlist(Bill b) {
			billlist.add(b);
		}
}
